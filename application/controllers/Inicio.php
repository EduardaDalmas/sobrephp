<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	
	public function index()
	{
		$this->load->view('menu');
		$this->load->view('inicio');
	}

	public function sobre()
	{
		// sugiro carregar o menu aqui, e não copiar e colar todo o código
		// html como tu fez, depois se tu altera o menu, 
		// precisa alterar em todas as pagina que tu copiou e colou
		
		$this->load->view('menu');
		$this->load->view('sobre');
	}

}
